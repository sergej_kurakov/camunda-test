package org.example

import org.camunda.bpm.engine.RuntimeService
import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.springframework.stereotype.Component

@Component("sendTask1")
class SendTask1(private val runtimeService: RuntimeService) : JavaDelegate {

    override fun execute(execution: DelegateExecution) {
        runtimeService
            .createMessageCorrelation("message1")
            .setVariable("mainProcessId", execution.processInstanceId)
            .correlate()
    }
}
