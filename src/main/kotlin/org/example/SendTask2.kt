package org.example

import org.camunda.bpm.engine.RuntimeService
import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.springframework.stereotype.Component

@Component("sendTask2")
class SendTask2(private val runtimeService: RuntimeService) : JavaDelegate {

    override fun execute(execution: DelegateExecution) {
        runtimeService
            .createMessageCorrelation("message2")
            .processInstanceId(execution.getVariable("mainProcessId").toString())
            .correlate()
    }
}
